import { Component } from '@angular/core';
import { DatePipe } from '@angular/common';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
/**
 * Generated class for the NotificationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {

  notification: any = {
    id: '',
    description: '',
    when: ''
  }

  private notifications : any[] = [
    {
      id: '1',
      description: 'Você ganhou o badge Estratégia Parabéns !',
      when: new Date()
    },
    {
      id: '2',
      description: 'Você ganhou o badge União Parabéns !',
      when: new Date()
    },
    {
      id: '3',
      description: 'Você ganhou o badge Pikachu Parabéns !',
      when: new Date()
    },
    {
      id: '4',
      description: 'Seu desempenho aumentou 20% !',
      when: new Date()
    },
    {
      id: '1',
      description: 'Você ganhou o badge Estratégia Parabéns !',
      when: new Date()
    },
    {
      id: '2',
      description: 'Você ganhou o badge União Parabéns !',
      when: new Date()
    },
    {
      id: '3',
      description: 'Você ganhou o badge Pikachu Parabéns !',
      when: new Date()
    },
    {
      id: '4',
      description: 'Seu desempenho aumentou 20% !',
      when: new Date()
    },
    {
      id: '1',
      description: 'Você ganhou o badge Estratégia Parabéns !',
      when: new Date()
    },
    {
      id: '2',
      description: 'Você ganhou o badge União Parabéns !',
      when: new Date()
    },
    {
      id: '3',
      description: 'Você ganhou o badge Pikachu Parabéns !',
      when: new Date()
    },
    {
      id: '4',
      description: 'Seu desempenho aumentou 20% !',
      when: new Date()
    },
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams) 
  {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationsPage');
  }

}

