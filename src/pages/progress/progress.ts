import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Assets } from '../loader/assets';

/**
 * 
 */
@IonicPage()
@Component({
  selector: 'page-progress',
  templateUrl: 'progress.html',
})
export class ProgressPage {

  /*****************************************************************
   *                         ATTRIBUTES
   *****************************************************************/

   /**
    * 
    */
   ranges: any[] = [];

  /**
   * 
   */
  badges: any[] = [
    {
      image: Assets.IMG_PATH_BADGE_STRATEGY,
      title: "Estrategista",
      when: new Date(),
    },

    {
      image: Assets.IMG_PATH_BADGE_UNION,
      title: "União",
      when: new Date(),
    },

    {
      image: Assets.IMG_PATH_BADGE_COINS,
      title: "Give my money, please!",
      when: new Date(),
    },
  ];

  /*****************************************************************
  *                        CONSTRUCTOR
  *****************************************************************/
  
  /**
   * 
   */
  constructor( public navCtrl: NavController, public navParams: NavParams ) {
    let range: any = {
      title: "Hoje",
      date: new Date(),
      badges: this.badges,
    };

    this.ranges.push( range );
  }

  /*****************************************************************
  *                         BEHAVIORS
  *****************************************************************/

  /**
   * 
   */
  randomBadge() {
    let CURRENT_DATE = new Date().getTime();
    this.ranges = [];

    let lastDate;
    for( let i = 0; i < 3; i++ ) {
      let date = new Date( CURRENT_DATE - ( 1000 * 3600 * 24 * Math.floor( ( Math.random() * 31 ) + 1 ) ) );
      let range: any = { date: date };

      if( lastDate == null ) {
        this.ranges.push( range );
      }
      else {
        if( this.diffDays( date, lastDate ) > 0 ) {
          this.ranges.push( range );
        }
      }

      lastDate = date;
    }
    
    this.ranges.sort( ( range, range2 ) => { 
      if( range.date.getTime() < range2.date.getTime() ) return 1;
      if( range.date.getTime() > range2.date.getTime() ) return -1;

      return 0;
    });

    for( let range of this.ranges ) {
      let badges = [];

      let maxLoop = Math.floor( ( Math.random() * 4 ) + 1 );
      for( let i = 1; i <= maxLoop; i++ ) {
        let badge = {
          image: Assets.getRandomBadgeImage(),
          title: Math.floor( ( Math.random() * 10000000000 ) + 1 ),
          when: range.date,
        };

        badges.push( badge );
      }

      let dateDiff = this.diffDays( range.date, new Date() );
      range.title = dateDiff == 0 ? 'Hoje' : ( dateDiff > 1 ? dateDiff + " dias atrás" : "Ontem" );
      range.badges = badges;
    }
  }

  /**
   * 
   * @param date 
   * @param date2 
   */
  diffDays( date: Date, date2: Date ) {
    let timeDiff = Math.abs( date.getTime() - date2.getTime() );
    return Math.ceil( timeDiff / ( 1000 * 3600 * 24) );
  }

}
