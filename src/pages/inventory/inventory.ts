import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Assets } from '../loader/assets';

/**
 * 
 */
@IonicPage()
@Component({
  selector: 'page-inventory',
  templateUrl: 'inventory.html',
})
export class InventoryPage {
  
  /*****************************************************************
   *                         ATTRIBUTES
   *****************************************************************/

   /**
    * 
    */
  badges: any[] = [
    {
      image: Assets.IMG_PATH_BADGE_STRATEGY,
      title: "Estrategista",
      when: new Date(),
    },

    {
      image: Assets.IMG_PATH_BADGE_UNION,
      title: "União",
      when: new Date(),
    },

    {
      image: Assets.IMG_PATH_BADGE_COINS,
      title: "Give my money, please!",
      when: new Date(),
    },
  ];

  /*****************************************************************
   *                        CONSTRUCTOR
   *****************************************************************/

  /**
   * 
   */
  constructor( public navCtrl: NavController, public navParams: NavParams ) {
  }

  /*****************************************************************
   *                         BEHAVIORS
   *****************************************************************/

  /**
   * 
   */
  randomBadge() {
    for( let badge of this.badges ) 
    {
      badge.image = Assets.getRandomBadgeImage();
      badge.title = Math.floor( ( Math.random() * 10000000000 ) + 1 );
      badge.when  = new Date( Math.floor( ( Math.random() * new Date().getTime() ) + new Date().getTime()/10 ) );
    }
  }
}
