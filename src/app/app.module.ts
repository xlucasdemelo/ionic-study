import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HelloIonicPage } from '../pages/hello-ionic/hello-ionic';
import { ItemDetailsPage } from '../pages/item-details/item-details';
import { TeamPage } from '../pages/team/team';
import { InventoryPage } from '../pages/inventory/inventory';
import { PerformancePage } from '../pages/performance/performance';
import { ProgressPage } from '../pages/progress/progress';
import { ListPage } from '../pages/list/list';
import { UserPage } from '../pages/user/user';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NotificationsPage } from '../pages/notifications/notifications';
import { FeedPage } from '../pages/feed/feed';

@NgModule({
  declarations: [
    MyApp,
    HelloIonicPage,
    ItemDetailsPage,
    ListPage,
    TeamPage,
    InventoryPage,
    PerformancePage,
    ProgressPage,
    UserPage,
    NotificationsPage,
    FeedPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HelloIonicPage,
    ItemDetailsPage,
    ListPage,
    TeamPage,
    InventoryPage,
    PerformancePage,
    ProgressPage,
    UserPage,
    NotificationsPage,
    FeedPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
