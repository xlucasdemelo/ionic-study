import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Assets } from '../loader/assets';

/**
 * 
 */
@IonicPage()
@Component({
  selector: 'page-team',
  templateUrl: 'team.html',
})
export class TeamPage {

  /*****************************************************************
   *                         ATTRIBUTES
   *****************************************************************/

   /**
    * 
    */
  userTeam: any = {
    rank: 5,
    points: 4800,
    image: Assets.getTrophyForRank( 5 ),
  }

  /**
   * 
   */
  members: any[] = [];
  member: any = {
    level: 2,
    name: "Henrique Damasco",
    image: Assets.IMG_PATH_AVATAR_1,
    
    team: {
      rank: 5,
      points: 4800,
      image: Assets.IMG_PATH_IRON_TROPHY,
    }
  }

  /*****************************************************************
   *                         CONSTRUCTOR
   *****************************************************************/

  /**
   * 
   * @param navCtrl 
   * @param navParams 
   */
  constructor( public navCtrl: NavController, public navParams: NavParams ) 
  {
    this.members.push( Object.assign( {}, this.member ) );
  }

  /*****************************************************************
   *                         BEHAVIORS
   *****************************************************************/

  /**
   * 
   */
  randomTeamInfo() 
  {
    this.userTeam.rank = Math.floor( ( Math.random() * 5 ) + 1 );
    this.userTeam.points = Math.floor( ( Math.random() * 5200 ) + 1 );
    this.userTeam.image = Assets.getTrophyForRank( this.userTeam.rank );
    
    if( this.members.length < 3 )
    {
      let team = Object.assign( {}, this.member.team );
      this.member.team = team;
      this.members.push( Object.assign( {}, this.member ) );
    }

    for( let member of this.members ) {
      member.image = Assets.getRandomAvatarImage();
      member.name = Math.floor( ( Math.random() * 1000000 ) + 1 );
      member.level = Math.floor( ( Math.random() * 8 ) + 1 );

      member.team.rank = Math.floor( ( Math.random() * 5 ) + 1 );
      member.team.points = Math.floor( ( Math.random() * 5200 ) + 1 );
      member.team.image = Assets.getTrophyForRank( member.team.rank );
    }
  }
}
