import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * 
 */
@IonicPage()
@Component({
  selector: 'page-performance',
  templateUrl: 'performance.html',
})
export class PerformancePage {

  level: any;

  constructor( public navCtrl: NavController, public navParams: NavParams ) {
    this.level = this.navParams.get( 'level' );
  }

  ionViewDidLoad() {
    console.log( 'ionViewDidLoad PerformancePage' );
  }

  /**
   * @return 0~100% to level up
   */
  getCurrentLevelPercentage(): any {
    return ( this.level.currentExp / this.level.nextLevelExp ) * 100;
  }

}
