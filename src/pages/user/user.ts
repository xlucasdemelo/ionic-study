import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { PerformancePage } from '../performance/performance';
import { InventoryPage } from '../inventory/inventory';
import { TeamPage } from '../team/team';
import { ProgressPage } from '../progress/progress';
import { Assets } from '../loader/assets';

/**
 * 
 */
@IonicPage()
@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
export class UserPage {

  /*****************************************************************
   *                         ATTRIBUTES
   *****************************************************************/
  
   /**
   * 
   */
  performanceTab: any = PerformancePage;
  inventoryTab: any = InventoryPage;
  teamTab: any = TeamPage;
  progressTab: any = ProgressPage;
 
  /**
   * User info
   */
  user: any = {
    image: Assets.IMG_PATH_AVATAR_1,
    jobFunction: "PMO",
    name: "Rodrigo da Silva",
  };

  /**
   * User's game info
   */
  game: any = {
    level: {
      image: Assets.IMG_PATH_LEVEL_1,
      currentLevel: "Level 8",
      currentExp: "4500",
      nextLevelExp: "5200",
    },

    position: {
      rank: 3,
      image: Assets.IMG_PATH_BRONZE_TROPHY,
      currentPosition: "3ª posição",
      currentPoints: "4200",
    },
    
    gold: {
      image: Assets.IMG_PATH_COIN,
      currentGold: "100",
      description: "Gamecoins",
    },
  };

  /*****************************************************************
   *                         CONSTRUCTOR
   *****************************************************************/

  /**
   * 
   */
  constructor( public navCtrl: NavController, public navParams: NavParams ) {
  }

  /*****************************************************************
   *                         BEHAVIORS
   *****************************************************************/

  /**
   * 
   */
  randomUserInfo() {
    this.user.image = Assets.getRandomAvatarImage();
    this.user.name = Math.floor( ( Math.random() * 10000000000 ) + 1 );
    this.user.jobFunction = Math.floor( ( Math.random() * 10000000 ) + 1 );
    
    this.game.level.image = Assets.getRandomLevelImage();
    this.game.level.currentLevel = "Level " + this.game.position.rank;
    this.game.level.currentExp = Math.floor( ( Math.random() * 5200 ) + 1 );
    
    this.game.position.rank = Math.floor( ( Math.random() * 5 ) + 1 );
    this.game.position.currentPosition = this.game.position.rank + "ª posição";
    this.game.position.currentPoints = Math.floor( ( Math.random() * 5000 ) + 1 );
    this.game.position.image = Assets.getTrophyForRank( this.game.position.rank );

    this.game.gold.currentGold = Math.floor( ( Math.random() * 1000 ) + 1 );
  }
}
