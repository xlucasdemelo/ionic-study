import { Component } from '@angular/core';
import { UserPage } from '../user/user';
import { InventoryPage } from '../inventory/inventory';

import { PerformancePage } from '../performance/performance';
import { NotificationsPage } from '../notifications/notifications';
import { FeedPage } from '../feed/feed';


@Component({
  selector: 'page-hello-ionic',
  templateUrl: 'hello-ionic.html'
})
export class HelloIonicPage {

  userTab: any = UserPage;
  notificationsTab: any = NotificationsPage;
  feedTab: any = FeedPage;

  constructor() {

  }

  getItems(nha:any){
    alert(nha);
  }

}
