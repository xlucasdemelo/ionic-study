/**
 * 
 */
export class Assets {

    /*****************************************************************
     *                          SVG PATHS
     *****************************************************************/
    public static ASSETS_PATH_TROPHY    = "./assets/svg/trophy/";
    public static IMG_PATH_GOLD_TROPHY      = Assets.ASSETS_PATH_TROPHY + "trophy-gold.svg";
    public static IMG_PATH_SILVER_TROPHY    = Assets.ASSETS_PATH_TROPHY + "trophy-silver.svg";
    public static IMG_PATH_BRONZE_TROPHY    = Assets.ASSETS_PATH_TROPHY + "trophy-bronze.svg";
    public static IMG_PATH_IRON_TROPHY      = Assets.ASSETS_PATH_TROPHY + "trophy-iron.svg";

    public static ASSETS_PATH_AVATAR    = "./assets/svg/avatar/";
    public static IMG_PATH_AVATAR_1     = Assets.ASSETS_PATH_AVATAR + "avatar1.svg";
    public static IMG_PATH_AVATAR_2     = Assets.ASSETS_PATH_AVATAR + "avatar2.svg";
    public static IMG_PATH_AVATAR_3     = Assets.ASSETS_PATH_AVATAR + "avatar3.svg";
    public static IMG_PATH_AVATAR_4     = Assets.ASSETS_PATH_AVATAR + "avatar4.svg";
    public static IMG_PATH_AVATAR_5     = Assets.ASSETS_PATH_AVATAR + "avatar5.svg";
    public static IMG_PATH_AVATAR_6     = Assets.ASSETS_PATH_AVATAR + "avatar6.svg";
    public static IMG_PATH_AVATAR_7     = Assets.ASSETS_PATH_AVATAR + "avatar7.svg";
    public static IMG_PATH_AVATAR_8     = Assets.ASSETS_PATH_AVATAR + "avatar8.svg";
    public static IMG_PATH_AVATAR_9     = Assets.ASSETS_PATH_AVATAR + "avatar9.svg";

    public static ASSETS_PATH_LEVEL     = "./assets/svg/levels/";
    public static IMG_PATH_LEVEL_1      = Assets.ASSETS_PATH_LEVEL + "level1.svg";
    public static IMG_PATH_LEVEL_2      = Assets.ASSETS_PATH_LEVEL + "level2.svg";
    public static IMG_PATH_LEVEL_3      = Assets.ASSETS_PATH_LEVEL + "level3.svg";
    public static IMG_PATH_LEVEL_4      = Assets.ASSETS_PATH_LEVEL + "level4.svg";
    public static IMG_PATH_LEVEL_5      = Assets.ASSETS_PATH_LEVEL + "level5.svg";
    public static IMG_PATH_LEVEL_6      = Assets.ASSETS_PATH_LEVEL + "level6.svg";
    public static IMG_PATH_LEVEL_7      = Assets.ASSETS_PATH_LEVEL + "level7.svg";
    public static IMG_PATH_LEVEL_8      = Assets.ASSETS_PATH_LEVEL + "level8.svg";
    public static IMG_PATH_LEVEL_9      = Assets.ASSETS_PATH_LEVEL + "level9.svg";
    public static IMG_PATH_LEVEL_10     = Assets.ASSETS_PATH_LEVEL + "level10.svg";

    public static BADGES_PATH = [];
    public static ASSETS_PATH_BADGE           = "./assets/svg/badge/";
    public static IMG_PATH_BADGE_MAP          = Assets.ASSETS_PATH_BADGE + "badge-map.svg";
    public static IMG_PATH_BADGE_IDEA         = Assets.ASSETS_PATH_BADGE + "badge-idea.svg";
    public static IMG_PATH_BADGE_LINK         = Assets.ASSETS_PATH_BADGE + "badge-link.svg";
    public static IMG_PATH_BADGE_MATH         = Assets.ASSETS_PATH_BADGE + "badge-math.svg";
    public static IMG_PATH_BADGE_MIND         = Assets.ASSETS_PATH_BADGE + "badge-mind.svg";
    public static IMG_PATH_BADGE_CHART        = Assets.ASSETS_PATH_BADGE + "badge-chart.svg";
    public static IMG_PATH_BADGE_COINS        = Assets.ASSETS_PATH_BADGE + "badge-coins.svg";
    public static IMG_PATH_BADGE_UNION        = Assets.ASSETS_PATH_BADGE + "badge-union.svg";
    public static IMG_PATH_BADGE_PENCIL       = Assets.ASSETS_PATH_BADGE + "badge-pencil.svg";
    public static IMG_PATH_BADGE_TARGET       = Assets.ASSETS_PATH_BADGE + "badge-target.svg";
    public static IMG_PATH_BADGE_BATTERY      = Assets.ASSETS_PATH_BADGE + "badge-battery.svg";
    public static IMG_PATH_BADGE_PENGUIN      = Assets.ASSETS_PATH_BADGE + "badge-penguin.svg";
    public static IMG_PATH_BADGE_STRATEGY     = Assets.ASSETS_PATH_BADGE + "badge-strategy.svg";

    public static ASSETS_PATH_COIN      = "./assets/svg/coin/";
    public static IMG_PATH_COIN         = Assets.ASSETS_PATH_COIN + "coin.svg";

    /**
     * 
     */
    public static getRandomBadgeImage() 
    {
      if( !Assets.BADGES_PATH.length )
      {
        Assets.BADGES_PATH.push( Assets.IMG_PATH_BADGE_MAP );
        Assets.BADGES_PATH.push( Assets.IMG_PATH_BADGE_IDEA );
        Assets.BADGES_PATH.push( Assets.IMG_PATH_BADGE_LINK );
        Assets.BADGES_PATH.push( Assets.IMG_PATH_BADGE_MATH );
        Assets.BADGES_PATH.push( Assets.IMG_PATH_BADGE_MIND );
        Assets.BADGES_PATH.push( Assets.IMG_PATH_BADGE_CHART );
        Assets.BADGES_PATH.push( Assets.IMG_PATH_BADGE_COINS );
        Assets.BADGES_PATH.push( Assets.IMG_PATH_BADGE_UNION );
        Assets.BADGES_PATH.push( Assets.IMG_PATH_BADGE_PENCIL );
        Assets.BADGES_PATH.push( Assets.IMG_PATH_BADGE_TARGET );
        Assets.BADGES_PATH.push( Assets.IMG_PATH_BADGE_PENGUIN );
        Assets.BADGES_PATH.push( Assets.IMG_PATH_BADGE_STRATEGY );
      }
        return Assets.BADGES_PATH[Math.floor( Math.random() * Assets.BADGES_PATH.length )];
    }

    /**
     * 
     */
    public static getRandomAvatarImage() 
    {
        return Assets.ASSETS_PATH_AVATAR + "avatar" + Math.floor( ( Math.random() * 9 ) + 1 ) + ".svg";
    }

    /**
     * 
     */
    public static getRandomLevelImage() 
    {
        return Assets.ASSETS_PATH_LEVEL + "level" + Math.floor( ( Math.random() * 9 ) + 1 ) + ".svg";
    }

    /**
     * 
     * @param rank 
     */
    public static getTrophyForRank( rank: any )
    {
        switch( rank )
        {
          case 1:
            return Assets.IMG_PATH_GOLD_TROPHY;
          
          case 2:
            return Assets.IMG_PATH_SILVER_TROPHY;
    
          case 3:
            return Assets.IMG_PATH_BRONZE_TROPHY;
    
          default:
            return Assets.IMG_PATH_IRON_TROPHY;
        }
    }
}