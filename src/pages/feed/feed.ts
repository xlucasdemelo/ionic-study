import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FeedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feed',
  templateUrl: 'feed.html',
})
export class FeedPage {

  private notifications : any[] = [
    {
      id: '1',
      description: 'Ganhou o badge Estratégia Parabéns !',
      who: 'Henrique damasco',
      when: new Date(),
      xp: '300+',
      level: '3+',
      badge: 'Estrategista'
    },
    {
      id: '1',
      description: 'Ganhou o badge Pikachu Parabéns !',
      who: 'lhavel',
      when: new Date(),
      xp: '2908+',
      level: '4+',
      badge: 'Pikachu'
    },
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeedPage');
  }

  getItems(nha:any){
    alert(nha);
  }

}
